from selenium import webdriver
import os
import unittest


browser = os.getenv('browser', 'firefox')
browsers = {
    'firefox': 'firefox',
    'chrome':  'chrome',
    'phantomjs': 'phantomjs',
    'safari': 'remote',
    'IE': 'remote'
}

chrome_path = '/Users/hemasudheer/Testing/Drivers/chromedriver/chromedriver'
firefox_gecko_path = '/Users/hemasudheer/Testing/Drivers/gecko/geckodriver'


class TestDriver(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if browser == 'chrome':
            cls.driver = webdriver.Chrome(executable_path=chrome_path)
        elif browser == 'firefox':
            cls.driver = webdriver.Firefox(executable_path=firefox_gecko_path)
        # other browsers can be done in the same way

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()