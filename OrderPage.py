from BasePage import BasePage
import time


loc = {
    "vita_logo" : "//a[@class='logo navbar-brand']",
    "login_button" : "//a[contains(text(),'Login')]",
    "feedback_button": "//div[@class='vm-feedback-button']//a",
    "order_now": "//button[contains(text(),'Order now')]",
    "input_location": "//input[contains(@placeholder,'Type in your current location...')]",
    "store_name": "//div[contains(@class,'scroll-items')]//div[{}]//div[1]//div[1]//h3[1]",
    "current_store_name": "//div[contains(@class,'store-location selected')]//h3",
    "store_button": "//div[contains(@class,'scroll-items')]//div[{}]//div[1]//div[2]//button[1]",
    "store_selected_confirmation": "//div[contains(@class,'store-location selected')]//img",
    "view_menu": "//button[contains(text(),'View Menu')]",
    "all_day": "//span[contains(text(),'All Day')]",
    "change_store": "//button[contains(@class,'vm-default change vm-button')]",
    "t&c": "//a[contains(text(),'Terms & Conditions')]",
    


}


class OrderPage(BasePage):

    def __init__(self, driver):
        self.driver = driver

    def load_landing_page(self):
        self.driver.get("https://frontend-staging.vitamojo.com/#/index")
        self.driver.implicitly_wait(10)
        assert self.driver.find_element_by_xpath(loc["vita_logo"]).is_displayed()
        assert self.driver.find_element_by_xpath(loc["login_button"]).is_displayed()
        assert self.driver.find_element_by_xpath(loc["feedback_button"]).is_displayed()

    def select_location(self, data):
        self.load_landing_page()
        self.press(loc["order_now"])
        self.driver.implicitly_wait(5)
        assert "location-select" in self.driver.current_url
        time.sleep(5)
        # method to check wait until for now static
        self.clear(loc["input_location"])
        self.type_and_enter(loc["input_location"], data['location'])
        time.sleep(5)
        selected_location = self.driver.find_element_by_xpath(loc["current_store_name"])
        print "Current Store is " + selected_location.text
        self.press(loc["view_menu"])
        time.sleep(5)
        assert "menu" in self.driver.current_url
        time.sleep(5)
        assert self.driver.find_element_by_xpath(loc["all_day"]).is_displayed()
        return True

    def change_location(self, data):
        stores = {1 : 'Spitalfields',
                  2 : 'St Pauls',
                  3 : 'Store A',
                  4 : 'Master store a',
                  5 : 'Store B'}
        self.load_landing_page()
        self.press(loc["change_store"])
        self.clear(loc["input_location"])
        self.type_and_enter(loc["input_location"], data['new_location'])
        self.press(loc["store_button"].format(3))
        selected_location = self.driver.find_element_by_xpath(loc["current_store_name"])
        print "Current Store is " + selected_location.text
        time.sleep(10)
        return True

    def check_terms_conditions(self):
        self.load_landing_page()
        self.press(loc["t&c"])
        self.driver.implicitly_wait(3)
        assert "terms-and-conditions" in self.driver.current_url
        terms_dict = {
            "static" : "//span[contains(text(), '{}')]"
        }
        terms = ['Accessing our Website', 'Prohibited', 'Uploading material', 'Linking to', 'Our liability',
                 'Intellectual Property Rights', 'Viruses and other offences', 'Suspension and termination',
                 'Jurisdiction and Applicable Law']
        count = 0
        for term in terms:
            assert self.driver.find_element_by_xpath(terms_dict["static"].format(term)).is_displayed()
            count += 1
        print "Total number of terms to check : " + str(len(terms))
        print "Total number of terms present:" + str(count)
        assert len(terms) == count
        return True


