
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver

    def type(self, locator, text):
        self.driver.find_element_by_xpath(locator).send_keys(text)

    def clear(self, locator):
        self.driver.find_element_by_xpath(locator).clear()

    def type_and_enter(self, locator, text):
        self.driver.find_element_by_xpath(locator).send_keys(text)
        self.driver.find_element_by_xpath(locator).send_keys(Keys.ENTER)

    def press(self, locator):
        self.driver.find_element_by_xpath(locator).click()

    def wait_until_appear(self, locator, timeout=60, silent=False):
        t_end = time.time() + timeout
        while time.time() < t_end:
            if self.driver.find_element_by_xpath(locator):
                return True
            time.sleep(2)
        if silent:
            return False

        raise NoSuchElementException(locator, timeout)

    def find_by_locator(self, locator, timeout=20, silent=False):

        elem_type, element = locator.split('=', 1)
        if elem_type == 'css':
            return self.driver.find_element_by_css_selector(element)
        elif elem_type == 'id':
            return self.driver.find_element_by_id(element)
        elif elem_type == 'name':
            return self.driver.find_element_by_name(element)
        elif elem_type == 'tag':
            return self.driver.find_element_by_tag(element)
        elif elem_type == 'xpath':
            return self.driver.find_element_by_xpath(element)

