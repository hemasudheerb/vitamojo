from BasePage import BasePage
from OrderPage import OrderPage
import time

loc = {
    "vita_logo" : "//a[@class='logo navbar-brand']",
    "login_button" : "//a[contains(text(),'Login')]",
    "email": "//input[@name='email']",
    "next_button": "//button[contains(text(),'Next')]",
    "checkout":"//button[contains(text(),'Continue to checkout')]"

}


class LoginPage(OrderPage, BasePage):

    def __init__(self, driver):
        self.driver = driver

    def login(self, data):
        self.load_landing_page()
        self.press(loc["login_button"])
        self.driver.implicitly_wait(3)
        self.type(loc["email"], data['email'])
        self.press(loc["next_button"])
        time.sleep(5)
        assert self.driver.find_element_by_xpath(loc["checkout"]).is_displayed()
        return True

