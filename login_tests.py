from OrderPage import OrderPage
from LoginPage import LoginPage
from TestDriver import TestDriver

test_data = {
    'location': 'barbican',
    'new_location': 'store a',
    'email': 'test@test5.com'
}


class LoginTests(TestDriver):

    def test_001_load_address_and_select_one(self):
        obj = OrderPage(self.driver)
        assert obj.select_location(test_data)

    def test_002_change_store(self):
        obj = OrderPage(self.driver)
        assert obj.change_location(test_data)

    def test_003_check_terms_conditions(self):
        obj = OrderPage(self.driver)
        assert obj.check_terms_conditions()

    def test_004_login(self):
        obj = LoginPage(self.driver)
        assert obj.login(test_data)
